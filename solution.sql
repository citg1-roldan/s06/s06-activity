-- 2.
-- a. The busy executive's database guide & you can combat computer stress
-- b. cooking with computers
-- c. Bennet, Abraham & Green, Marjorie
-- d. Algodata infosystem
-- e. busy executive's database guide, cooking with computers, straight talk about computers,
-- but is it user friendly, secrets of silicon valley, & net etiquitte 



-- 3. CREATING A DATABASE
CREATE DATABASE blog_db;
-- USERS
CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT primary key,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(300),
  datetime_created DATETIME NOT NULL
  
);
-- POSTS
CREATE TABLE posts (
  id INT NOT NULL AUTO_INCREMENT primary key,
  author_id INT NOT NULL,
  title VARCHAR(500) NOT NULL,
  content VARCHAR(5000) NOT NULL,
  datetime_posted DATETIME NOT NULL,
    CONSTRAINT fk_posts_users_id
	FOREIGN KEY(author_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT

  
);
-- POST_LIKES
CREATE TABLE post_likes (
  id INT NOT NULL AUTO_INCREMENT primary key,
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  datetime_liked DATETIME NOT NULL,
    CONSTRAINT fk_post_likes_posts_id
	FOREIGN KEY(post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
    CONSTRAINT fk_post_likes_users_id
	FOREIGN KEY(user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

-- POST_COMMENTS
CREATE TABLE post_comments (
  id INT NOT NULL AUTO_INCREMENT primary key,
  post_id INT NOT NULL,
  user_id INT NOT NULL,
  content VARCHAR(5000) NOT NULL,
  datetime_commented DATETIME NOT NULL,
    CONSTRAINT fk_post_comments_posts_id
	FOREIGN KEY(post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
    CONSTRAINT fk_post_comments_users_id
	FOREIGN KEY(user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);